FROM ubuntu

RUN apt-get update

RUN apt-get install -y python3-pip && pip install ansible

RUN mkdir /xrdp

COPY ./ /xrdp

RUN ansible-playbook /ansible/tests/test.yml